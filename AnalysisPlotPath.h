#pragma once

#include <memory>

#include "core/module/Module.hpp"
#include "objects/MostLikelyPath.hpp"
#include "objects/PhantomPath.hpp"

class TTree;

namespace corryvreckan {
    class AnalysisPlotPath : public Module {
    public:
        AnalysisPlotPath(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors);

        void initialize() override;
        StatusCode run(const std::shared_ptr<Clipboard>& clipboard) override;
        void finalize(const std::shared_ptr<ReadonlyClipboard>& clipboard) override;

    private:
        static constexpr const char* gGarametersTreeName = "parameters";
        static constexpr const char* gBeamParticleBranchName = "beamParticle";

        std::unique_ptr<MostLikelyPath> mMlpModel;
        std::string mInFileName;
        double mPhantomPosition;

        std::size_t mLastUpstreamIndex;
        // temporary for root
        TTree* mTree;
        int mEvent;
        double mX;
        double mY;
        double mZ;
        /** 0: actual path, 1: upstream, 2: mlp, 3: downstream */
        int mSection;
    };
} // namespace corryvreckan
