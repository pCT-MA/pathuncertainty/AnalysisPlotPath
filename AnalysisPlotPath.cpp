#include <TFile.h>

#include "AnalysisPlotPath.h"

namespace corryvreckan {
    AnalysisPlotPath::AnalysisPlotPath(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors)
        : Module(config, detectors), mEvent(0), mX(0), mY(0), mZ(0), mSection(0) {
        mInFileName = config.getPath("infile");
        mPhantomPosition = config.get<double>("phantom_position");
        std::size_t i = 0;
        for(auto detector : get_detectors()) {
            if(detector->origin().z() > mPhantomPosition) {
                break;
            }
            ++i;
        }
        mLastUpstreamIndex = i - 1;
    }

    void AnalysisPlotPath::initialize() {
        auto toCharge = [](std::string const& particle) {
            if(particle == "proton")
                return 1;
            if(particle == "helium" || particle == "helion")
                return 2;
            throw std::invalid_argument("Unknown particle name: " + particle);
        };
        TFile inFile(mInFileName.c_str());
        // find the particle name, to obtain rest energy
        auto parametersTree = dynamic_cast<TTree*>(inFile.Get(gGarametersTreeName));
        if(parametersTree == nullptr) {
            throw std::invalid_argument("Geant4EventLoader: Did not find "
                                        "parameters tree in input file.");
        }
        char beamParticle[100];
        parametersTree->SetBranchAddress(gBeamParticleBranchName, &beamParticle, nullptr);
        parametersTree->GetEntry(0);
        mMlpModel.reset(new MostLikelyPath(new Geant4KinematicTerm(mInFileName), toCharge(beamParticle)));

        mTree = new TTree("tree", "tree");
        mTree->Branch("event", &mEvent);
        mTree->Branch("x", &mX);
        mTree->Branch("y", &mY);
        mTree->Branch("z", &mZ);
        mTree->Branch("section", &mSection);
    }

    StatusCode AnalysisPlotPath::run(const std::shared_ptr<Clipboard>& clipboard) {
        auto convertPosition = [](XYZPoint const& p) { return Eigen::Vector3d(p.X(), p.Y(), p.Z()); };
        auto convertDirection = [](XYZVector const& p) { return Eigen::Vector2d(p.X() / p.Z(), p.Y() / p.Z()); };
        auto fillTree = [&](double x, double y, double z, int section) {
            mX = x;
            mY = y;
            mZ = z;
            mSection = section;
            mTree->Fill();
        };

        const auto tracks = clipboard->getData<Track>();
        const auto paths = clipboard->getData<Path>();

        if(!paths.empty()) {
            const auto path = paths.front();
            for(const auto& track : tracks) {
                const auto inZ = mPhantomPosition + path->mZ.front();
                const auto inPosition = convertPosition(track->getIntercept(inZ));
                const auto inDirection = track->getDirection(inZ);

                const auto outZ = mPhantomPosition + path->mZ.back();
                const auto outPosition = convertPosition(track->getIntercept(outZ));
                const auto outDirection = track->getDirection(outZ);

                // section -1: the detectors
                for(auto detector : get_detectors()) {
                    auto xyz = detector->getIntercept(track.get());
                    fillTree(xyz.x(), xyz.y(), xyz.z(), -1);
                }

                // section 0: the actual path
                for(std::size_t i = 0; i < Path::gNumberOfLayers; i++) {
                    fillTree(path->mX.at(i), path->mX.at(i), mPhantomPosition + path->mZ.at(i), 0);
                }

                // section 1: upstream track
                for(std::size_t i = 0; i <= mLastUpstreamIndex; i++) {
                    const auto z = get_detectors().at(i)->origin().z();
                    const auto intercept = track->getIntercept(z);
                    fillTree(intercept.x(), intercept.y(), z, 1);
                }
                {
                    const auto intercept = track->getIntercept(inZ);
                    fillTree(intercept.x(), intercept.y(), inZ, 1);
                }

                // section 2: mlp model
                fillTree(inPosition.x(), inPosition.y(), inPosition.z(), 2);
                for(std::size_t i = 1; i < Path::gNumberOfLayers - 1; i++) {
                    const auto z = mPhantomPosition + paths.front()->mZ.at(i);
                    const auto xy = mMlpModel->ProjectXY(
                        {inPosition, outPosition, convertDirection(inDirection), convertDirection(outDirection)}, z);
                    fillTree(xy.x(), xy.y(), z, 2);
                }
                fillTree(outPosition.x(), outPosition.y(), outPosition.z(), 2);

                // section 3: downstream track
                {
                    const auto intercept = track->getIntercept(outZ);
                    fillTree(intercept.x(), intercept.y(), outZ, 3);
                }
                for(std::size_t i = mLastUpstreamIndex + 1; i < get_detectors().size(); i++) {
                    const auto z = get_detectors().at(i)->origin().z();
                    const auto intercept = track->getIntercept(z);
                    fillTree(intercept.x(), intercept.y(), z, 3);
                }

                ++mEvent;
            }
        }
        return StatusCode::Success;
    }

    void AnalysisPlotPath::finalize(const std::shared_ptr<ReadonlyClipboard>&) {
        getROOTDirectory()->WriteObject(mTree, "tree");
        delete mTree;
        mTree = nullptr;
    }
} // namespace corryvreckan
